package com.eyeo.isthisbullsht.api

import com.eyeo.isthisbullsht.data.UrlConstraint
import org.xml.sax.Attributes
import org.xml.sax.helpers.DefaultHandler

class RatingGroupsParser : DefaultHandler() {

    companion object {
        private const val KEY_RATING_GROUP = "dr"
        private const val KEY_URL_CONSTRAINTS = "iriset"
        private const val KEY_URL_HOSTS = "includehosts"
        private const val KEY_URL_STARTING_PATHS = "includepathstartswith"
        private const val KEY_RATING = "ns1:rating"
    }

    val ratingMap = mutableMapOf<String, MutableMap<String, String>>()

    private val urlConstraints = arrayListOf<UrlConstraint>()
    private var rating: String? = null
    private val urlHosts = arrayListOf<String>()
    private val urlStartingPaths = arrayListOf<String>()

    private var inRating = false
    private var inUrlHosts = false
    private var inUrlStartingPaths = false

    override fun startElement(uri: String?, localName: String?, qName: String?, attributes: Attributes?) {
        when {
            KEY_RATING_GROUP == qName -> urlConstraints.clear()
            KEY_URL_CONSTRAINTS == qName -> {
                urlHosts.clear()
                urlStartingPaths.clear()
            }
            KEY_URL_HOSTS == qName -> inUrlHosts = true
            KEY_URL_STARTING_PATHS == qName -> inUrlStartingPaths = true
            KEY_RATING == qName -> inRating = true
        }
    }

    override fun endElement(uri: String?, localName: String?, qName: String?) {
        when {
            KEY_RATING_GROUP == qName -> {
                for (urlConstraint in urlConstraints) {
                    for (host in urlConstraint.hosts) {
                        val pathMap = ratingMap[host] ?: mutableMapOf()
                        if(urlConstraint.startingPaths.isEmpty()) {
                            pathMap[""] = rating!!
                        } else {
                            for(startingPath in urlConstraint.startingPaths) {
                                pathMap[startingPath] = rating!!
                            }
                        }
                        ratingMap[host] = pathMap
                    }
                }
            }
            KEY_URL_CONSTRAINTS == qName -> urlConstraints.add(
                UrlConstraint(
                    urlHosts.toMutableList(),
                    urlStartingPaths.toMutableList()
                )
            )
            KEY_URL_HOSTS == qName -> inUrlHosts = false
            KEY_URL_STARTING_PATHS == qName -> inUrlStartingPaths = false
            KEY_RATING == qName -> inRating = false
        }

    }

    override fun characters(ch: CharArray?, start: Int, length: Int) {
        super.characters(ch, start, length)
        when {
            inRating -> rating = String(ch!!, start, length)
            inUrlHosts -> urlHosts.addAll(String(ch!!, start, length).split(" ").toList())
            inUrlStartingPaths -> urlStartingPaths.addAll(String(ch!!, start, length).split(" ").toList())
        }
    }
}